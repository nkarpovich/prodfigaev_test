<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;

Loc::loadMessages(__FILE__);

class karpovich_customcatalog extends CModule{
    public function __construct(){

        if(file_exists(__DIR__."/version.php")){

            $arModuleVersion = array();

            include_once(__DIR__."/version.php");

            $this->MODULE_ID 		   = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION 	   = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME 		   = Loc::getMessage("CUSTOM_CATALOG_NAME");
            $this->MODULE_DESCRIPTION  = Loc::getMessage("CUSTOM_CATALOG_DESCRIPTION");
            $this->PARTNER_NAME 	   = Loc::getMessage("CUSTOM_CATALOG_PARTNER_NAME");
        }

        return false;
    }
    public function DoInstall(){
        ModuleManager::registerModule($this->MODULE_ID);
        $this->InstallDB();
    }
    public function DoUnInstall(){
        ModuleManager::UnRegisterModule($this->MODULE_ID);
        $this->UninstallDB();
    }
    public function InstallDB(){
        global $DB;
        $sql = file_get_contents(dirname(__FILE__).'/db/mysql/install.sql');
        $DB->Query($sql);
    }
    public function UninstallDB(){
        global $DB;
        $sql = file_get_contents(dirname(__FILE__).'/db/mysql/uninstall.sql');
        $DB->Query($sql);
    }
}