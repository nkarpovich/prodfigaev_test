<?
namespace Karpovich\CustomCatalog;

use Bitrix\Main\Entity;

class MyCustomCatalogTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'my_custom_catalog';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            new Entity\StringField('NAME', array(
                'required' => true,
            )),
            new Entity\DateField('DATE_INSERT', array(
                'required' => true,
                'default_value' => new \Bitrix\Main\Type\Date
            ))
        );
    }
}
