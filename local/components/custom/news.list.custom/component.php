<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Karpovich\CustomCatalog\MyCustomCatalogTable;

if (\Bitrix\Main\Loader::includeModule('karpovich.customcatalog'))
{
    /* Можно раскомментировать строки ниже для быстрого добавления 2-х тестовых элементов в таблицу, чтобы потестировать */

    /*$result = MyCustomCatalogTable::add(array(
        'NAME' => 'Test1'
    ));
    $result2 = MyCustomCatalogTable::add(array(
        'NAME' => 'Test2'
    ));*/

    $query = MyCustomCatalogTable::getList(array(
        'cache' => array(
            'ttl' => $arParams["CACHE_TIME"],
            'cache_joins' => true,
        )
    ));
    $arResult = $query->fetchAll();
    $this->IncludeComponentTemplate();
}

